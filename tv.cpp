#include "tv.h"

TV::TV(int min, int max){
    if(min<1 || max<min)
        throw std::runtime_error("TV constructor faild");
    minChannel = min;
    maxChannel = max;
    currState = OFF;
    currChannel = 1;
}

bool TV::turn_on(){
    if(currState == OFF)
    {
        currState = ON;
        return true;
    }
    else
        return false;
}

bool TV::turn_off(){
    if(currState == ON)
    {
        currState = OFF;
        return true;
    }
    else
        return false;
}

bool TV::setChanel(int channel){
    if(channel>=minChannel && channel<=maxChannel)
    {
        currChannel = channel;
        return true;
    }
    else
        return false;
}


int TV::getCurrChannel(){
    return currChannel;
}

bool TV::isTurnedOn(){
    return currState;
}

int TV::getMaxChannel(){
    return maxChannel;
}

int TV::getMinChannel(){
    return minChannel;
}

#ifndef THREAD_H
#define THREAD_H
#include <iostream>
#include "thread_body.h"
#include <pthread.h>
using namespace std;

class Thread
{
    pthread_t thread;


public:

    Thread(){
        //std::cout<<"Thread created"<<std::endl;
    }

    virtual void run(){}

    int Start()
    {
        void *data=reinterpret_cast<void*>(this);
        return pthread_create(&thread, NULL, thread_body<Thread>,data);
    }

    int Join()
    {
        return pthread_join(thread,NULL);
    }
    int Cancel()
    {
        return pthread_cancel(thread);
    }
    ~Thread()
    {
        Cancel();
        //std::cout<<"Thread destroyed"<<std::endl;
    }

};

#endif // THREAD_H

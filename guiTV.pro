#-------------------------------------------------
#
# Project created by QtCreator 2013-12-15T15:00:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = guiTV
TEMPLATE = app
LIBS += -pthread
QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp\
        mainwindow.cpp \
    threadtv.cpp \
    tv.cpp \
    guitv.cpp

HEADERS  += mainwindow.h \
    thread.h \
    thread_body.h \
    threadtv.h \
    tv.h \
    guitv.h

FORMS    += mainwindow.ui \
    guitv.ui

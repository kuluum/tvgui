#include "guitv.h"
#include "ui_guitv.h"
#include "QDebug"
guiTV::guiTV(QWidget *parent) :
    QMainWindow(parent), ThreadTV(1,99),
    ui(new Ui::guiTV)
{
    ui->setupUi(this);
    isCrazy = false;
    connect(&timer, SIGNAL(timeout()), this, SLOT(setTvInfo()));
    if(this->getState()){
        ui->stateButton->setText("OFF");
        ui->channelLine->setText(tr("%1").arg(this->getChannel()));
    }
    else{
        ui->stateButton->setText("ON");
        ui->channelLine->setText("OFF");
    }
}
guiTV::~guiTV()
{
    delete ui;
 //   std::cout<<"guiTV destruct"<<std::endl;
}

void guiTV::on_minusBtn_clicked()
{
    if(this->getState()){
        this->setChanel( this->getChannel() - 1 );
        ui->channelLine->setText(tr("%1").arg(this->getChannel()));
    }
    else ui->channelLine->setText("OFF");
}
void guiTV::on_plusBtn_clicked()
{
    if(this->getState()){
        this->setChanel( this->getChannel() + 1 );
        ui->channelLine->setText(tr("%1").arg(this->getChannel()));
    }    else ui->channelLine->setText("OFF");
}
void guiTV::on_setBtn_clicked()
{
    if(this->getState()){
        QString str = ui->channelLine->text();
        int channel = str.toInt();
        this->setChanel(channel);
        ui->channelLine->setText(tr("%1").arg(this->getChannel()));
    }   else ui->channelLine->setText("OFF");
}

void guiTV::on_crazyBtn_clicked()
{
    if(!isCrazy){
        ui->minusBtn->setEnabled(false);
        ui->plusBtn->setEnabled(false);
        ui->setBtn->setEnabled(false);
        ui->stateButton->setEnabled(false);
        this->Start();
        timer.start(1000);//
        isCrazy = true;
    }
    else{
        ui->minusBtn->setEnabled(true);
        ui->plusBtn->setEnabled(true);
        ui->setBtn->setEnabled(true);
        ui->stateButton->setEnabled(true);
        this->Cancel();
        timer.stop();
        isCrazy = false;
    }


}

void guiTV::on_stateButton_clicked()
{
    if(this->getState()){
        ui->stateButton->setText("ON");
        this->turn_off();
        ui->channelLine->setText("OFF");
    }
    else{
        ui->stateButton->setText("OFF");
        ui->channelLine->setText(tr("%1").arg(this->getChannel()));
        this->turn_on();
    }
}


void guiTV::setTvInfo()
{
    qDebug()<<"setTV";
    if(this->getState()){
        ui->channelLine->setText(tr("%1").arg(this->getChannel()));
    }
    else ui->channelLine->setText("OFF");
}

void guiTV::closeEvent(QCloseEvent *event)
{
    this->~guiTV();
    event->accept();
}

#ifndef THREAD_BODY_H
#define THREAD_BODY_H
template <class T>
void * thread_body(void* data)
{
    T *t =reinterpret_cast<T*>(data);
    t->run();
    return 0;
}
#endif // THREAD_BODY_H

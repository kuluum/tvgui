#ifndef TV_H
#define TV_H
#include <stdexcept>
class TV
{
    int minChannel, maxChannel, currChannel;
    enum State
    {
        ON = true,
        OFF = false
    } currState;

public:
    TV(int, int);
    bool turn_on();
    bool turn_off();
    bool setChanel(int channel);

protected:
    int getCurrChannel();
    int getMaxChannel();
    int getMinChannel();
    bool isTurnedOn();
};

#endif // TV_H

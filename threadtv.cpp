#include "threadtv.h"

ThreadTV::ThreadTV(int min, int max):TV(min, max), Thread(){
    srand (time(NULL));
}

void ThreadTV::run(){
    //std::cout<<"run ThreadTv"<<std::endl;
    while(1)
    {
        sleep(5);
        channelMutex.lock();
        changeChannel();
        channelMutex.unlock();
        //std::cout<<"currChannel="<<getCurrChannel()<<std::endl;
        sleep(5);
        stateMutex.lock();
        changeState();
        stateMutex.unlock();
        //std::cout<<"currState="<<isTurnedOn()<<std::endl;
    }
}

int ThreadTV::randChannel(){
    return getMinChannel() + rand() % (getMaxChannel() - getMinChannel());
}

void ThreadTV::changeChannel(){
    if(isTurnedOn())
        setChanel(randChannel());
    else
        setChanel(0);//
}

void ThreadTV::changeState(){
    if(isTurnedOn()){
        if(rand()%100 < 10)
            turn_off();
    } else {
        if(rand()%100 > 10)
            turn_on();
    }

}

int ThreadTV::getChannel()
{
    int channel;
    channelMutex.lock();
    channel = getCurrChannel();
    channelMutex.unlock();
    return channel;
}

bool ThreadTV::getState()
{
    bool state;
    stateMutex.lock();
    state = isTurnedOn();
    stateMutex.unlock();
    return state;
}

ThreadTV::~ThreadTV()
{
    //std::cout<<"ThreadTV destruct"<<std::endl;
}

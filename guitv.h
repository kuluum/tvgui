#ifndef GUITV_H
#define GUITV_H

#include <QMainWindow>
#include "threadtv.h"
#include <QTimer>
#include <QCloseEvent>
namespace Ui {
class guiTV;
}

class guiTV : public QMainWindow, public ThreadTV
{
    Q_OBJECT
    QTimer timer;
    bool isCrazy;
    void closeEvent(QCloseEvent *event);
public:
    explicit guiTV(QWidget *parent = 0);
    ~guiTV();
    
private slots:
    void on_minusBtn_clicked();

    void on_plusBtn_clicked();

    void on_setBtn_clicked();

    void on_crazyBtn_clicked();

    void on_stateButton_clicked();

    void setTvInfo();
private:
    Ui::guiTV *ui;
};

#endif // GUITV_H

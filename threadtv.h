#ifndef THREADTV_H
#define THREADTV_H
#include"tv.h"
#include"thread.h"
#include <time.h>
#include "unistd.h"
#include <mutex>

class ThreadTV: public TV, public Thread
{
    int randChannel();
    void changeChannel();
    void changeState();
    std::mutex channelMutex;
    std::mutex stateMutex;
public:
    ThreadTV(int, int);
    ~ThreadTV();
    void run();
    int getChannel();
    bool getState();
};

#endif // THREADTV_H

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addBtn_clicked()
{
    guiTV *newTV = new guiTV();
    newTV->show();
}

void MainWindow::on_closeBtn_clicked()
{
    qApp->closeAllWindows();
}
